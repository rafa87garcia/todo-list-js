// get
const input = document.querySelector("input");
const button_add = document.querySelector(".btn-add");
const ul = document.querySelector("ul");
const emply = document.querySelector(".empty");

// Crear funcion añadir los elementos a la lista

button_add.addEventListener("click", (e) => {
  e.preventDefault();

  const text = input.value;

  if (text !== "") {
    const li = document.createElement("li");
    const p = document.createElement("p");
    p.textContent = text;
    li.appendChild(p);
    li.appendChild(addDeleteBtn()); //hoisting
    ul.appendChild(li);
  }
  input.value = "";
  emply.style.display = "none";
});

function addDeleteBtn() {
  const deleteBtn = document.createElement("button");
  deleteBtn.textContent = "X";
  deleteBtn.className = "btn-delete";
  deleteBtn.addEventListener("click", (e) => {
    const liParent = e.target.parentElement;
    liParent.remove();

    const lists = document.querySelectorAll("li");
    if ((lists.length) === 0) {
        emply.style.display = "block";
      }
  });

  return deleteBtn;
}
